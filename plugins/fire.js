import Vue from 'vue'
import firebase from 'firebase/app'

// console.log('fire.js')

if(!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyCtPbZSmSw9FRtD4o11R9YWslROc9FASBU",
    authDomain: "t-maker-5ece6.firebaseapp.com",
    databaseURL: "https://t-maker-5ece6.firebaseio.com",
    projectId: "t-maker-5ece6",
    storageBucket: "",
    messagingSenderId: "631133714730",
    appId: "1:631133714730:web:ea74401f7f5786e00bc653"
  })
}

Vue.prototype.$user = (e,i) => {
  return firebase.auth().currentUser
}