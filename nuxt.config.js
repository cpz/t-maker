const pkg = require('./package')


module.exports = {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    script: [
      // { src: 'https://draggabilly.desandro.com/draggabilly.pkgd.js' },
      // { src: 'https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js' }
    ],
    link: [
      // { rel: 'stylesheet', href: '/style.css' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/fire.js', // firebase database
    // '@/plugins/debounce.js',
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // ['nuxt-buefy', { // Doc: https://buefy.github.io/#/documentation
    //   // defaultTooltipType: 'is-info',
    //   defaultNoticeQueue: false,
    //   defaultSnackbarDuration: 1600
    // }],
  ],
  // cache service worker
  workbox: {
  },

  /*
  ** Build configuration
  */
  build: {
    analyze: true,
    // or
    // analyze: {
    //   analyzerMode: 'static'
    // }
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      
    }
  }
}
